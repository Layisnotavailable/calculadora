import sys


def calculo(args):
    if len(args) > 3:
        numbers = []
        operator = ''
        operators = ['+', '-', '*', '/']
        args.pop(0)
        for arg in args:
           if arg in operators:
               operator = arg
           else:
               numbers.append(arg)
        if operator != '':
            numero_um = int(numbers[0])
            numero_dois = int(numbers[1])
            if operator == '-':
                a = numero_um - numero_dois
                return a
            elif operator == '/':
                if numero_um == 0 or numero_dois == 0:
                    print("Não se divide por 0 seu jumento")
                else:
                    a = numero_um / numero_dois
                    return a
            elif operator == '+':
                a = numero_um + numero_dois
                return a
            else:
                a = numero_um * numero_dois
                return a
        else:
            print("Está faltando operador")
    
    else:
        print("Não da pra faze conta faltando um parâmetro seu tanso!")


    #arg[0] vai ser o primeiro número
    #arg[1] vai ser a operação
    #arg[2] vai ser o segundo número
    #dentro do for pegamos os parâmetros fazemos os cálculos e retornamos o resultado


if __name__ == '__main__':
    calculo(sys.argv)