import calculadora


def soma():
    result = calculadora.calculo(['2', '+', '4'])
    assert result == 6


def sub():
    result = calculadora.calculo(['2', '-', '2'])
    assert result == 0


def div():
    result = calculadora.calculo(['4', '/', '2'])
    assert result == 2
    try:
        result = calculadora.calculo(['2', '/', '0'])
    except ZeroDivisionError:
        print("Não dividirais por zero - algum versiculo da biblia")
        assert True
    else:
        assert False


def mult():
    result = calculadora.calculo(['2', '*', '2'])
    assert result == 4


if __name__ == '__main__':
    soma()
    sub()
    div()
    mult()
